'use strict';

/**
 * Dogs.js controller
 *
 * @description: A set of functions called "actions" for managing `Dogs`.
 */

module.exports = {

  /**
   * Retrieve dogs records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.dogs.search(ctx.query);
    } else {
      return strapi.services.dogs.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a dogs record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.dogs.fetch(ctx.params);
  },

  /**
   * Count dogs records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.dogs.count(ctx.query);
  },

  /**
   * Create a/an dogs record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.dogs.add(ctx.request.body);
  },

  /**
   * Update a/an dogs record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.dogs.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an dogs record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.dogs.remove(ctx.params);
  }
};
