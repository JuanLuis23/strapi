
# REQUIREMENTS TO INSTALL STRAPI.JS
***

Before you go any further, there are some requirements to make sure the installation works. Ensure these technologies are installed on your computer:

  - [Node.js at least v 9.0.0](https://nodejs.org/en/download/)
  - [Npm at least v 5.0.0](https://docs.npmjs.com/getting-started/installing-node)
  - [MongoDB at least v 3.4](https://docs.mongodb.com/manual/installation/)

***Regulary when you install nodejs in your computer NPM already come by default with the package.***

***

## Steps to follow for installing strapi.js and its requeriments in the Vagrant box
Use the CLI for installing all these technologies. Run these commands:

```sh
vagrant up
vagrant provision
vagrant ssh
```
For making sure that have these technologies installed with the following commands: 

Nodejs
```sh
$ node -v
```
Npm
```sh
$ npm -v
```
MongoDB
```sh
$ mongod -v
```
Strapi.js
```sh
$ strapi -v
```

## Clone
***
Clone the repository using the following command:
```sh
$ cd strapi
```

Install dependencies and modules
```sh
$ npm i
```

#### Launch the server
```sh
$ strapi start
```
Open your browser -> (http://localhost:1337/admin) and create the first user.

#### Creating an User
Open your browser in the link indicated before. It will appear you a login form. Fill in it with the followign fields 

```
Username: 123
Password: 123456
```

## Getting access token from strapi
***
Now for getting an access token you need to do a POST petition and sending the following parameters in Json Format.
- [Install postman](https://www.getpostman.com/)
-- Open it and select POST petition 
-- Write in the URL (http://localhost:1337/auth/local)
-- In the body, select json/application like request type
-- Write the next json:

```json
{
	"identifier":"yourUserNameOrYourEmail",
	"password":"yourPassword"
}
```
- Launch it and the request should be something like this:
```json
{
    "jwt": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YmM1MTUyNWQxZmQwMjFiYjQ1YzBkNDIiLCJpYXQiOjE1Mzk5MDYyODUsImV4cCI6MTU0MjQ5ODI4NX0.sSNAd0aeTKiMYp0-O8QUPq7ZzPm8AOmvm5Z9MazjDR0",
    "user": {
        "_id": "yourID",
        "confirmed": true,
        "blocked": false,
        "username": "YourUserName",
        "email": "YourEmail",
        "role": {
            "_id": "ROLEID",
            "name": "Administrator",
            "description": "These users have all access in the project.",
            "type": "root",
            "__v": 0
        },
        "__v": 0
    }
}
```
- Copy the JWT and save it.

#### Doing an api call
Now, for doing a sample api call:

1. Open a new tab in [Postman](https://www.getpostman.com/) and write the next url --> http://localhost:1337/users/
2. Make sure that is selected Method GET
2. Select Headers and write the token copied:
```Headers
Authorization: Bearer yourTokenCopied
``` 

And ready, if all is ok your browser will show you a ArrayJson Format with the info of the every user.

```json
[
    {
        "_id": "IDUSER",
        "confirmed": true,
        "blocked": false,
        "username": "username",
        "email": "email",
        "provider": "local",
        "role": {
            "_id": "ROLEID",
            "name": "Administrator",
            "description": "These users have all access in the project.",
            "type": "root",
            "__v": 0
        },
        "__v": 0
    }
]
```